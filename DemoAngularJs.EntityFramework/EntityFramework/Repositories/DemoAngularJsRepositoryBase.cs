﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace DemoAngularJs.EntityFramework.Repositories
{
    public abstract class DemoAngularJsRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<DemoAngularJsDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected DemoAngularJsRepositoryBase(IDbContextProvider<DemoAngularJsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class DemoAngularJsRepositoryBase<TEntity> : DemoAngularJsRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected DemoAngularJsRepositoryBase(IDbContextProvider<DemoAngularJsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
