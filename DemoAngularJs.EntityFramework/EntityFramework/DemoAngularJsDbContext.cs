﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using DemoAngularJs.Authorization.Roles;
using DemoAngularJs.Authorization.Users;
using DemoAngularJs.MultiTenancy;

namespace DemoAngularJs.EntityFramework
{
    public class DemoAngularJsDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //TODO: Define an IDbSet for your Entities...
        #region [IDbSets of Entities]
        public virtual IDbSet<Product.Product> Products { get; set; }
        public virtual IDbSet<UnitOfMeasurement.UnitOfMeasurement> UnitOfMeasurements { get; set; }
        #endregion

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public DemoAngularJsDbContext()
            : base(DemoAngularJs.Helper.busConstant.Settings.DataBase.SqlServer.Connections.ConnectionString.DEFAULT)
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in DemoAngularJsDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of DemoAngularJsDbContext since ABP automatically handles it.
         */
        public DemoAngularJsDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public DemoAngularJsDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public DemoAngularJsDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }
    }
}
