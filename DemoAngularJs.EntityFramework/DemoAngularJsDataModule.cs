﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using DemoAngularJs.EntityFramework;

namespace DemoAngularJs
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(DemoAngularJsCoreModule))]
    public class DemoAngularJsDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DemoAngularJsDbContext>());

            Configuration.DefaultNameOrConnectionString = DemoAngularJs.Helper.busConstant.Settings.DataBase.SqlServer.Connections.ConnectionString.DEFAULT;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
