namespace DemoAngularJs.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Tbl_UnitOfMeasurement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tbl_Unit_Of_Measurement",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Unit_Of_Measurement_Name = c.String(),
                        Unit_Of_Measurement_Description = c.String(),
                        Is_Default = c.Boolean(nullable: false),
                        TenantId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnitOfMeasurement_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UnitOfMeasurement_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TenantId)
                .Index(t => t.IsDeleted);
            
            AddColumn("dbo.Tbl_Product", "UnitOfMeasurementId", c => c.Long(nullable: false));
            CreateIndex("dbo.Tbl_Product", "UnitOfMeasurementId");
            AddForeignKey("dbo.Tbl_Product", "UnitOfMeasurementId", "dbo.Tbl_Unit_Of_Measurement", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tbl_Product", "UnitOfMeasurementId", "dbo.Tbl_Unit_Of_Measurement");
            DropIndex("dbo.Tbl_Unit_Of_Measurement", new[] { "IsDeleted" });
            DropIndex("dbo.Tbl_Unit_Of_Measurement", new[] { "TenantId" });
            DropIndex("dbo.Tbl_Product", new[] { "UnitOfMeasurementId" });
            DropColumn("dbo.Tbl_Product", "UnitOfMeasurementId");
            DropTable("dbo.Tbl_Unit_Of_Measurement",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnitOfMeasurement_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UnitOfMeasurement_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
