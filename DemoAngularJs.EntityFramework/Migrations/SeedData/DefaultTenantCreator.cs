using System.Linq;
using DemoAngularJs.EntityFramework;
using DemoAngularJs.MultiTenancy;

namespace DemoAngularJs.Migrations.SeedData
{
    public class DefaultTenantCreator
    {
        private readonly DemoAngularJsDbContext _context;

        public DefaultTenantCreator(DemoAngularJsDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserAndRoles();
        }

        private void CreateUserAndRoles()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                _context.Tenants.Add(new Tenant {TenancyName = Tenant.DefaultTenantName, Name = Tenant.DefaultTenantName});
                _context.SaveChanges();
            }
        }
    }
}
