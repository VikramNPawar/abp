﻿using DemoAngularJs.EntityFramework;
using EntityFramework.DynamicFilters;

namespace DemoAngularJs.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly DemoAngularJsDbContext _context;

        public InitialHostDbBuilder(DemoAngularJsDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
