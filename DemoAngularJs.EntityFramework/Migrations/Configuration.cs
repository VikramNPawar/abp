using System.Data.Entity.Migrations;
using Abp.MultiTenancy;
using Abp.Zero.EntityFramework;
using DemoAngularJs.Helper;
using DemoAngularJs.Migrations.SeedData;
using EntityFramework.DynamicFilters;

namespace DemoAngularJs.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<DemoAngularJs.EntityFramework.DemoAngularJsDbContext>, IMultiTenantSeed
    {
        public AbpTenantBase Tenant { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = busConstant.Misc.FALSE;
            ContextKey = "DemoAngularJs";
        }

        protected override void Seed(DemoAngularJs.EntityFramework.DemoAngularJsDbContext context)
        {
            context.DisableAllFilters();

            if (Tenant == null)
            {
                //Host seed
                new InitialHostDbBuilder(context).Create();

                //Default tenant seed (in host database).
                new DefaultTenantCreator(context).Create();
                new TenantRoleAndUserBuilder(context, 1).Create();
            }
            else
            {
                //You can add seed for tenant databases and use Tenant property...
            }

            context.SaveChanges();
        }
    }
}
