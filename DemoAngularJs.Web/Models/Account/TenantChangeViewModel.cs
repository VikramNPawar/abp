using Abp.AutoMapper;
using DemoAngularJs.Sessions.Dto;

namespace DemoAngularJs.Web.Models.Account
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}