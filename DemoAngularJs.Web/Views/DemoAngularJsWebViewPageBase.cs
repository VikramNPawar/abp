﻿using Abp.Web.Mvc.Views;

namespace DemoAngularJs.Web.Views
{
    public abstract class DemoAngularJsWebViewPageBase : DemoAngularJsWebViewPageBase<dynamic>
    {

    }

    public abstract class DemoAngularJsWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected DemoAngularJsWebViewPageBase()
        {
            LocalizationSourceName = DemoAngularJsConsts.LocalizationSourceName;
        }
    }
}