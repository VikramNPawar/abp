﻿using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Web.Mvc;
using Abp.Web.SignalR;
using Abp.Zero.Configuration;
using DemoAngularJs.Api;
using Hangfire;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DemoAngularJs.Web
{
    [DependsOn(
        typeof(DemoAngularJsDataModule),
        typeof(DemoAngularJsApplicationModule),
        typeof(DemoAngularJsWebApiModule),
        typeof(AbpWebSignalRModule),
        typeof(AbpHangfireModule), //- ENABLE TO USE HANGFIRE INSTEAD OF DEFAULT JOB MANAGER
        typeof(AbpWebMvcModule))]
    public class DemoAngularJsWebModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Enable database based localization
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            //Configure navigation/menu
            Configuration.Navigation.Providers.Add<DemoAngularJsNavigationProvider>();

            //Configure Hangfire - ENABLE TO USE HANGFIRE INSTEAD OF DEFAULT JOB MANAGER
            Configuration.BackgroundJobs.UseHangfire(configuration =>
            {
                configuration.GlobalConfiguration.UseSqlServerStorage(DemoAngularJs.Helper.busConstant.Settings.DataBase.SqlServer.Connections.ConnectionString.DEFAULT);
            });




            //Enable to automatically log all interactions within the application. It can record intended method calls with caller info and arguments.
            //Basically, the saved fields are: Related tenant id, caller user id, called service name(the class of the called method), called method name, 
            //execution parameters(serialized into JSON), execution time, execution duration(in milliseconds), the client's IP address, the client's computer name and the exception(if the method throws an exception).
            Configuration.Auditing.IsEnabled = true;

        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
