﻿var App = App || {};
(function () {

    var appLocalizationSource = abp.localization.getSource('DemoAngularJs');
    App.localize = function () {
        return appLocalizationSource.apply(this, arguments);
    };

})(App);