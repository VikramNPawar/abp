﻿(function () {
    angular.module('app').controller('app.views.products.index', [
        '$scope', '$log', '$timeout', '$uibModal', 'abp.services.app.product',
        function ($scope, $log, $timeout, $uibModal, productService) {
            var vm = this;

            vm.PagedResultRequestDto = {};
            vm.products = [];
            vm.maxSize = 11;
            vm.PageSizes = [
                { "ID": 10, "Value": "10" },
                { "ID": 25, "Value": "25" },
                { "ID": 50, "Value": "50" },
                { "ID": 100, "Value": "100" },
                { "ID": -1, "Value": "All" }
            ];
            vm.totalItems = 0;
            vm.itemsPerPage = 10;
            vm.currentPage = 1;
            vm.SkipCount = 0;
            vm.MaxResultCount = vm.itemsPerPage;
            vm.Sorting = 'Id';
            vm.SortDirection = false;
            vm.showSart = 0;
            vm.showEnd = 0;
            vm.showOutOf = 0;
            /*Start Of Private Methods*/

            function applyFilteringSortingAndPagingParameters() {
                vm.PagedResultRequestDto.PageNumber = vm.currentPage;
                vm.PagedResultRequestDto.SortDirection = vm.SortDirection ? 'asc' : 'desc';
                vm.PagedResultRequestDto.Sorting = vm.Sorting;
                vm.PagedResultRequestDto.SkipCount = vm.SkipCount;
                vm.PagedResultRequestDto.MaxResultCount = vm.MaxResultCount;
            }

            function getProducts() {
                vm.isTableLoading = true;
                applyFilteringSortingAndPagingParameters();
                productService.getAll(vm.PagedResultRequestDto).then(function (result) {
                    vm.totalItems = result.data.totalCount > 0 ? result.data.totalCount : 0;
                    vm.showSart = vm.SkipCount + 1;
                    vm.showEnd = vm.SkipCount + (result.data.items.length < vm.itemsPerPage ? vm.totalItems - vm.SkipCount : vm.itemsPerPage);
                    vm.showOutOf = vm.totalItems;
                    vm.products = result.data.items;
                    debugger;
                    vm.isTableLoading = false;
                });
            }

            /*End Of Private Methods*/



            /* Start Of Public Methods*/

            vm.setPage = function (pageNo) {
                vm.currentPage = pageNo;
            };

            vm.pageSizeChanged = function () {
                vm.setPage(1);
                vm.MaxResultCount = vm.itemsPerPage;
                vm.SkipCount = vm.itemsPerPage * (vm.currentPage <= 0 ? 0 : vm.currentPage - 1);
                getProducts();
                // $log.log('Page changed to: ' + $scope.currentPage);
            };

            vm.pageChanged = function () {
                vm.MaxResultCount = vm.itemsPerPage;
                vm.SkipCount = vm.itemsPerPage * (vm.currentPage <= 0 ? 0 : vm.currentPage - 1);
                getProducts();
                // $log.log('Page changed to: ' + $scope.currentPage);
            };




            vm.sortBy = function (colunmName) {
                vm.SortDirection = vm.Sorting === colunmName ? !vm.SortDirection : true;
                vm.Sorting = colunmName;
                getProducts();
            };

            vm.openProductCreationModal = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/products/createModal.cshtml',
                    controller: 'app.views.products.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.rendered.then(function () {
                    $.AdminBSB.input.activate();
                });

                modalInstance.result.then(function () {
                    getProducts();
                });
            };

            vm.openProductEditModal = function (product) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/Main/views/products/editModal.cshtml',
                    controller: 'app.views.products.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        id: function () {
                            return product.id;
                        }
                    }
                });

                modalInstance.rendered.then(function () {
                    $timeout(function () {
                        $.AdminBSB.input.activate();
                    }, 0);
                });

                modalInstance.result.then(function () {
                    getProducts();
                });
            };

            vm.delete = function (product) {
                abp.message.confirm(
                    "Delete product '" + product.productName + "'?",
                    function (result) {
                        if (result) {
                            productService.delete({ id: product.id })
                                .then(function () {
                                    abp.notify.info("Deleted product: " + product.productName);
                                    getProducts();
                                });
                        }
                    });
            };

            vm.refresh = function () {
                getProducts();
            };

            /* End Of Public Methods*/

            getProducts();
        }
    ]);
})();