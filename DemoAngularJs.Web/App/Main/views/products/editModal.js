﻿(function () {
    angular.module('app').controller('app.views.products.editModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.product', 'id',
        function ($scope, $uibModalInstance, productService, id) {
            var vm = this;

            vm.product = {
                isActive: true
            };

            vm.unitofmeasurements = [];

            var setAssignedUnitOfMeasurement = function (product, unitofmeasurements) {
                debugger;
                for (var i = 0; i < unitofmeasurements.length; i++) {
                    var unitofmeasurement = unitofmeasurements[i];
                    role.isAssigned = $.inArray(unitofmeasurement.name, user.unitofmeasurement) >= 0;
                }
            };

            function init() {
                productService.getUnitOfMeasurements()
                    .then(function (result) {
                        vm.unitofmeasurements = result.data.items;

                        productService.get({ id: id })
                            .then(function (result) {
                                vm.product = result.data;
                                //setAssignedUnitOfMeasurement(vm.product, vm.unitofmeasurements);
                            });
                    });
            }

            vm.save = function () {
                productService.update(vm.product)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };

            init();
        }
    ]);
})();