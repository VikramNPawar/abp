﻿(function () {
    angular.module('app').controller('app.views.products.createModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.product',
        function ($scope, $uibModalInstance, productService) {
            var vm = this;

            vm.product = {};
            vm.product.selected_unitOfMeasurementId = -1;
            vm.unitofmeasurements = [];

            function getUnitOfMeasurements() {
                productService.getUnitOfMeasurements()
                    .then(function (result) {
                        vm.unitofmeasurements = result.data.items;
                    });
            }

            vm.save = function () {
                productService.create(vm.product)
                    .then(function () {
                        abp.notify.info(App.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                    });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss({});
            };


            getUnitOfMeasurements();
        }
    ]);
})();