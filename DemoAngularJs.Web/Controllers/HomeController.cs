﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace DemoAngularJs.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : DemoAngularJsControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }
	}
}