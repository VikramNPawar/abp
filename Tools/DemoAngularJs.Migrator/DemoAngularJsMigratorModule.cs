using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using DemoAngularJs.EntityFramework;

namespace DemoAngularJs.Migrator
{
    [DependsOn(typeof(DemoAngularJsDataModule))]
    public class DemoAngularJsMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<DemoAngularJsDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}