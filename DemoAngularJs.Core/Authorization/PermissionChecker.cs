﻿using Abp.Authorization;
using DemoAngularJs.Authorization.Roles;
using DemoAngularJs.Authorization.Users;

namespace DemoAngularJs.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
