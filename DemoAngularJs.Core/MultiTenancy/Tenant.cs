﻿using Abp.MultiTenancy;
using DemoAngularJs.Authorization.Users;

namespace DemoAngularJs.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}