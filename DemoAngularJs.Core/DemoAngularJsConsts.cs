﻿namespace DemoAngularJs
{
    public class DemoAngularJsConsts
    {
        public const string LocalizationSourceName = "DemoAngularJs";

        public const bool MultiTenancyEnabled = true;
    }
}