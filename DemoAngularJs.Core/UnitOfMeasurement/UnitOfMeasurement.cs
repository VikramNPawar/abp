﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.UnitOfMeasurement
{
    [Table("Tbl_Unit_Of_Measurement", Schema = "dbo")]
    public class UnitOfMeasurement : FullAuditedEntity<long>, IMayHaveTenant
    {
        public const int MaxUnitOfMeasurementNameLength = 10;
        public const int MaxUnitOfMeasurementDescriptionLength = 100;

        [Column("Unit_Of_Measurement_Name")]
        public string UnitOfMeasurementName { get; set; }

        [Column("Unit_Of_Measurement_Description")]
        public string UnitOfMeasurementDescription { get; set; }

        [Column("Is_Default")]
        public bool IsDefault { get; set; }

        public int? TenantId { get; set; }


        /// <summary>
        /// Collection of products which are having this unit of measurement.
        /// </summary>
        [ForeignKey("UnitOfMeasurementId")]
        public virtual ICollection<DemoAngularJs.Product.Product> Products { get; set; }
    }
}
