﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.Product
{
    [Table("Tbl_Product", Schema = "dbo")]
    public class Product : FullAuditedEntity<long>, IMayHaveTenant
    {
        public const int MaxProductNameLength = 100;
        public const int MaxProductDescriptionLength = 5000;

        public string ProductName { get; set; }

        public string ProductDescription { get; set; }

        public int? TenantId { get; set; }

        /// <summary>
        /// Current product have this unit of measurement.
        /// </summary>
        [ForeignKey("UnitOfMeasurementId")]
        public virtual DemoAngularJs.UnitOfMeasurement.UnitOfMeasurement UnitOfMeasurement { get; set; }
        public long UnitOfMeasurementId { get; set; }

    }
}
