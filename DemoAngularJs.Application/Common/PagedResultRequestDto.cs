﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.Common
{
    public class PagedResultRequestDto: Abp.Application.Services.Dto.PagedAndSortedResultRequestDto
    {
        public int PageNumber { get; set; }

        public string SortDirection { get; set; }
    }
}
