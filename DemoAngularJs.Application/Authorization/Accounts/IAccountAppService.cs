﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DemoAngularJs.Authorization.Accounts.Dto;

namespace DemoAngularJs.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
