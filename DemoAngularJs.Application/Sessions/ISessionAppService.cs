﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DemoAngularJs.Sessions.Dto;

namespace DemoAngularJs.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
