﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DemoAngularJs.Products.Dto;
using DemoAngularJs.UnitOfMeasurements.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.Products
{
    public interface IProductAppService : IAsyncCrudAppService<ProductDto, long, Common.PagedResultRequestDto, CreateProductDto, UpdateProductDto>
    {
        //ListResultDto<UnitOfMeasurementDto> GetUnitOfMeasurements();
        Task<ListResultDto<UnitOfMeasurementDto>> GetUnitOfMeasurements();


        //UnitOfMeasurementDto GetUnitOfMeasurementByProduct(Product.Product product);
        Task<UnitOfMeasurementDto> GetUnitOfMeasurementByProduct(Product.Product product);
    }
}
