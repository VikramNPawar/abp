﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using DemoAngularJs.Authorization;
using DemoAngularJs.Products.Dto;
using DemoAngularJs.UnitOfMeasurements.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.Products
{

    [AbpAuthorize(PermissionNames.Pages_Products)]
    public class ProductAppService : AsyncCrudAppService<Product.Product, ProductDto, long, Common.PagedResultRequestDto, CreateProductDto, UpdateProductDto>, IProductAppService
    {

        private readonly IRepository<Product.Product, long> _productRepository;
        private readonly IRepository<UnitOfMeasurement.UnitOfMeasurement, long> _unitOfMeasurementRepository;
       
        public ProductAppService(
            IRepository<Product.Product, long> productRepository,
            IRepository<UnitOfMeasurement.UnitOfMeasurement, long> unitOfMeasurementRepository)
            : base(productRepository)
        {
            _productRepository = productRepository;
            _unitOfMeasurementRepository = unitOfMeasurementRepository;         
        }

        public override Task<ProductDto> Get(EntityDto<long> input)
        {
            return base.Get(input);
        }

        protected override IQueryable<Product.Product> CreateFilteredQuery(Common.PagedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override IQueryable<Product.Product> ApplySorting(IQueryable<Product.Product> query, Common.PagedResultRequestDto input)
        {
            query = query.OrderBy(input.Sorting + " " + input.SortDirection);
            var returnValue = query;
            return returnValue;
        }

        public override Task<PagedResultDto<ProductDto>> GetAll(Common.PagedResultRequestDto input)
        {
            var returnValue = base.GetAll(input);
            return returnValue;
        }

        protected override IQueryable<Product.Product> ApplyPaging(IQueryable<Product.Product> query, Common.PagedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        public override async Task<ProductDto> Create(CreateProductDto input)
        {
            CheckCreatePermission();

            var product = ObjectMapper.Map<Product.Product>(input);

            product.TenantId = AbpSession.TenantId;

            _productRepository.Insert(product);

            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(product);
        }

        public override async Task<ProductDto> Update(UpdateProductDto input)
        {
            CheckUpdatePermission();

            var product = await _productRepository.GetAsync(input.Id);

            MapToEntity(input, product);

            return await Get(input);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var product = await _productRepository.GetAsync(input.Id);
            await _productRepository.DeleteAsync(product);
        }

        protected override void MapToEntity(UpdateProductDto input, Product.Product product)
        {
            ObjectMapper.Map(input, product);
        }

        //public UnitOfMeasurementDto GetUnitOfMeasurementByProductAsync(Product.Product product)
        //{
        //    var unitOfMeasurement = _unitOfMeasurementRepository.Get(product.UnitOfMeasurementId);
        //    product.UnitOfMeasurement = unitOfMeasurement;
        //    var productDto = ObjectMapper.Map<UnitOfMeasurementDto>(unitOfMeasurement);
        //    return productDto;
        //}

        public async Task<UnitOfMeasurementDto> GetUnitOfMeasurementByProduct(Product.Product product)
        {
            var unitOfMeasurement = await _unitOfMeasurementRepository.GetAsync(product.UnitOfMeasurementId);
            product.UnitOfMeasurement = unitOfMeasurement;
            return ObjectMapper.Map<UnitOfMeasurementDto>(unitOfMeasurement);
        }

        //public ListResultDto<UnitOfMeasurementDto> GetUnitOfMeasurementsAsync()
        //{
        //    try
        //    {
        //        var unitofmeasurements = _unitOfMeasurementRepository.GetAllList();
        //        var l = ObjectMapper.Map<List<UnitOfMeasurementDto>>(unitofmeasurements);
        //        var r = new ListResultDto<UnitOfMeasurementDto>(l);
        //        return r;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public async Task<ListResultDto<UnitOfMeasurementDto>> GetUnitOfMeasurements()
        {

            var unitofmeasurements = await _unitOfMeasurementRepository.GetAllListAsync();
            return new ListResultDto<UnitOfMeasurementDto>(ObjectMapper.Map<List<UnitOfMeasurementDto>>(unitofmeasurements));

        }
    }
}
