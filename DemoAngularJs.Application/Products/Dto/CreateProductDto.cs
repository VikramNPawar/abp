﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using DemoAngularJs.Authorization.Users;
using System.ComponentModel.DataAnnotations;

namespace DemoAngularJs.Products.Dto
{
    [AutoMapTo(typeof(DemoAngularJs.Product.Product))]
    public class CreateProductDto : FullAuditedEntityDto<long>
    {
        [Required]
        [StringLength(DemoAngularJs.Product.Product.MaxProductNameLength)]
        public string ProductName { get; set; }

        [Required]
        [StringLength(DemoAngularJs.Product.Product.MaxProductDescriptionLength)]
        public string ProductDescription { get; set; }

        [Required]      
        public long UnitOfMeasurementId { get; set; }
    }
}