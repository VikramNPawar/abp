﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using DemoAngularJs.Authorization.Users;
using System;
using System.ComponentModel.DataAnnotations;

namespace DemoAngularJs.Products.Dto
{
    [AutoMapFrom(typeof(DemoAngularJs.Product.Product))]
    public class ProductDto : FullAuditedEntityDto<long>
    {
        [Required]
        [StringLength(DemoAngularJs.Product.Product.MaxProductNameLength)]
        public string ProductName { get; set; }

        [Required]
        [StringLength(DemoAngularJs.Product.Product.MaxProductDescriptionLength)]
        public string ProductDescription { get; set; }

        [Required]
        public long UnitOfMeasurementId { get; set; }


        public virtual DemoAngularJs.UnitOfMeasurements.Dto.UnitOfMeasurementDto UnitOfMeasurementDto { get; set; }
    }
}
