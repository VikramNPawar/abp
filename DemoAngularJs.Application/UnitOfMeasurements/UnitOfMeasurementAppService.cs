﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using DemoAngularJs.Authorization;
using DemoAngularJs.Helper;
using DemoAngularJs.UnitOfMeasurement;
using DemoAngularJs.UnitOfMeasurements.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;


namespace DemoAngularJs.UnitOfMeasurements
{
    [AbpAuthorize(PermissionNames.Pages_UnitOfMeasurements)]
    public class UnitOfMeasurementAppService : AsyncCrudAppService<UnitOfMeasurement.UnitOfMeasurement, UnitOfMeasurementDto, long, Common.PagedResultRequestDto, CreateUnitOfMeasurementDto, UpdateUnitOfMeasurementDto>, IUnitOfMeasurementAppService
    {


        private readonly IRepository<UnitOfMeasurement.UnitOfMeasurement, long> _unitOfMeasurementRepository;
        private readonly IRepository<Product.Product, long> _productRepository;

        public UnitOfMeasurementAppService(
           IRepository<DemoAngularJs.UnitOfMeasurement.UnitOfMeasurement, long> unitOfMeasurementRepository,
           IRepository<DemoAngularJs.Product.Product, long> productRepository)
            : base(unitOfMeasurementRepository)
        {
            _unitOfMeasurementRepository = unitOfMeasurementRepository;
            _productRepository = productRepository;
        }



        public override Task<UnitOfMeasurementDto> Get(EntityDto<long> input)
        {
            return base.Get(input);
        }

        protected override IQueryable<UnitOfMeasurement.UnitOfMeasurement> CreateFilteredQuery(Common.PagedResultRequestDto input)
        {
            return base.CreateFilteredQuery(input);
        }

        protected override IQueryable<UnitOfMeasurement.UnitOfMeasurement> ApplySorting(IQueryable<UnitOfMeasurement.UnitOfMeasurement> query, Common.PagedResultRequestDto input)
        {
            query = query.OrderBy(input.Sorting + " " + input.SortDirection);
            var returnValue = query;
            return returnValue;
        }

        public override Task<PagedResultDto<UnitOfMeasurementDto>> GetAll(Common.PagedResultRequestDto input)
        {
            var returnValue = base.GetAll(input);
            return returnValue;
        }

        protected override IQueryable<UnitOfMeasurement.UnitOfMeasurement> ApplyPaging(IQueryable<UnitOfMeasurement.UnitOfMeasurement> query, Common.PagedResultRequestDto input)
        {
            return base.ApplyPaging(query, input);
        }

        public override async Task<UnitOfMeasurementDto> Create(CreateUnitOfMeasurementDto input)
        {
            CheckCreatePermission();

            var unitofmeasurement = ObjectMapper.Map<UnitOfMeasurement.UnitOfMeasurement>(input);

            unitofmeasurement.TenantId = AbpSession.TenantId;

            _unitOfMeasurementRepository.Insert(unitofmeasurement);

            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(unitofmeasurement);
        }

        public override async Task<UnitOfMeasurementDto> Update(UpdateUnitOfMeasurementDto input)
        {
            CheckUpdatePermission();

            var unitofmeasurement = await _unitOfMeasurementRepository.GetAsync(input.Id);

            MapToEntity(input, unitofmeasurement);

            return await Get(input);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            var unitOfMeasurement = await _unitOfMeasurementRepository.GetAsync(input.Id);
            await _unitOfMeasurementRepository.DeleteAsync(unitOfMeasurement);
        }

        protected override void MapToEntity(UpdateUnitOfMeasurementDto input, UnitOfMeasurement.UnitOfMeasurement unitOfMeasurement)
        {
            ObjectMapper.Map(input, unitOfMeasurement);
        }






        public ListResultDto<Products.Dto.ProductDto> GetProducts(UnitOfMeasurement.UnitOfMeasurement unitOfMeasurement)
        {
            var products = _productRepository.GetAllList(product => product.UnitOfMeasurementId == unitOfMeasurement.Id);
            unitOfMeasurement.Products = products;
            return new ListResultDto<Products.Dto.ProductDto>(ObjectMapper.Map<List<Products.Dto.ProductDto>>(products));
        }
        public async Task<ListResultDto<Products.Dto.ProductDto>> GetProductsAsync(UnitOfMeasurement.UnitOfMeasurement unitOfMeasurement)
        {
            var products = await _productRepository.GetAllListAsync(product => product.UnitOfMeasurementId == unitOfMeasurement.Id);
            unitOfMeasurement.Products = products;
            return new ListResultDto<Products.Dto.ProductDto>(ObjectMapper.Map<List<Products.Dto.ProductDto>>(products));
        }
    }
}
