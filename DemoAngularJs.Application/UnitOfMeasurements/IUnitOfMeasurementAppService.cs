﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DemoAngularJs.UnitOfMeasurements.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.UnitOfMeasurement
{
    public interface IUnitOfMeasurementAppService : IAsyncCrudAppService<UnitOfMeasurementDto, long, DemoAngularJs.Common.PagedResultRequestDto, CreateUnitOfMeasurementDto, UpdateUnitOfMeasurementDto>
    {

        ListResultDto<DemoAngularJs.Products.Dto.ProductDto> GetProducts(DemoAngularJs.UnitOfMeasurement.UnitOfMeasurement unitOfMeasurement);
        Task<ListResultDto<DemoAngularJs.Products.Dto.ProductDto>> GetProductsAsync(DemoAngularJs.UnitOfMeasurement.UnitOfMeasurement unitOfMeasurement);
    }
}
