﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.UnitOfMeasurements.Dto
{
    [AutoMapFrom(typeof(UnitOfMeasurement.UnitOfMeasurement)), AutoMapTo(typeof(UnitOfMeasurement.UnitOfMeasurement))]    
    public class UnitOfMeasurementDto : FullAuditedEntityDto<long>
    {
       
        public string UnitOfMeasurementName { get; set; }

     
        public string UnitOfMeasurementDescription { get; set; }

        
        public bool IsDefault { get; set; }

        public int? TenantId { get; set; }

        public virtual List<Products.Dto.ProductDto> Products { get; set; }
    }
}
