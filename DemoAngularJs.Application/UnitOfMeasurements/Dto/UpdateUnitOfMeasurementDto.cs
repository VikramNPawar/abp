﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAngularJs.UnitOfMeasurements.Dto
{
    [AutoMapTo(typeof(DemoAngularJs.UnitOfMeasurement.UnitOfMeasurement))]
    public class UpdateUnitOfMeasurementDto : FullAuditedEntityDto<long>
    {
        [Required]
        [StringLength(DemoAngularJs.UnitOfMeasurement.UnitOfMeasurement.MaxUnitOfMeasurementNameLength)]
        public string UnitOfMeasurementName { get; set; }

        [Required]
        [StringLength(DemoAngularJs.UnitOfMeasurement.UnitOfMeasurement.MaxUnitOfMeasurementDescriptionLength)]
        public string UnitOfMeasurementDescription { get; set; }

        [Required]
        public bool IsDefault { get; set; }

    }
}
