﻿using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Modules;
using DemoAngularJs.Authorization.Roles;
using DemoAngularJs.Authorization.Users;
using DemoAngularJs.Roles.Dto;
using DemoAngularJs.Users.Dto;
using System.Reflection;

namespace DemoAngularJs
{
    [DependsOn(typeof(DemoAngularJsCoreModule), typeof(AbpAutoMapperModule))]
    public class DemoAngularJsApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            // TODO: Is there somewhere else to store these, with the dto classes
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                // Role and permission
                cfg.CreateMap<Permission, string>().ConvertUsing(r => r.Name);
                cfg.CreateMap<RolePermissionSetting, string>().ConvertUsing(r => r.Name);

                cfg.CreateMap<CreateRoleDto, Role>().ForMember(x => x.Permissions, opt => opt.Ignore());
                cfg.CreateMap<RoleDto, Role>().ForMember(x => x.Permissions, opt => opt.Ignore());

                cfg.CreateMap<UserDto, User>();
                cfg.CreateMap<UserDto, User>().ForMember(x => x.Roles, opt => opt.Ignore());

                cfg.CreateMap<CreateUserDto, User>();
                cfg.CreateMap<CreateUserDto, User>().ForMember(x => x.Roles, opt => opt.Ignore());

                //cfg.CreateMap<UnitOfMeasurement.UnitOfMeasurement, UnitOfMeasurements.Dto.UnitOfMeasurementDto>();
                //cfg.CreateMap<List<UnitOfMeasurement.UnitOfMeasurement>, List<UnitOfMeasurements.Dto.UnitOfMeasurementDto>>();

            });
        }
    }
}
