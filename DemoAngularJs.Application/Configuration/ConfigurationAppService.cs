﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using DemoAngularJs.Configuration.Dto;

namespace DemoAngularJs.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : DemoAngularJsAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
