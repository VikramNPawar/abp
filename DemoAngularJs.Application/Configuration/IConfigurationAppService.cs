﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DemoAngularJs.Configuration.Dto;

namespace DemoAngularJs.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}