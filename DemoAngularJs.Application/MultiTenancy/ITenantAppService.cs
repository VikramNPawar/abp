﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DemoAngularJs.MultiTenancy.Dto;

namespace DemoAngularJs.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
